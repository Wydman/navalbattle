package com.SPP;


public class Board {
    private State[][] board;

    public Board(){
        board = new State[10][10];
        fillBoardVoid();
    }

    public State[][] getBoard() {
        return board;
    }

    public void setBoardIJ(int i, int j, State state) {
        board[i][j] = state;
    }

    public void fillBoardVoid(){
        for(int i = 0; i < 10; ++i){
            for(int j = 0; j < 10; ++j){
                setBoardIJ(i, j, State.VOID);
            }
        }
    }

}