package com.SPP;

public class Coordinates {
    private int xCoordinate;
    private int yCoordinate;
    private State status;

    public Coordinates(int x, int y) {
        xCoordinate = x;
        yCoordinate = y;
        status = status.VOID;
    }

    public void changeStatus(State status){
        this.status = status;
    }

    public State getStatus() {
        return status;
    }

    public int getxCoordinate() {
        return xCoordinate;
    }

    public int getyCoordinate() {
        return yCoordinate;
    }
}
