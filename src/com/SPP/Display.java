package com.SPP;

public class Display {

    public static String displayBoards(String name, Board board1, Board board2){
        StringBuilder result = new StringBuilder();
        result.append("--------------------------------------------------------").append("\t\t\t").append("--------------------------------------------------------")
                .append(System.lineSeparator())
                .append("|               Grille du joueur ").append(name).append(" ".repeat(22 - name.length()))
                .append("|").append("\t\t\t").append("|                  Grille de l'ennemi                  |")
                .append(System.lineSeparator())
                .append("+----+----+----+----+----+----+----+----+----+----+----+").append("\t\t\t").append("+----+----+----+----+----+----+----+----+----+----+----+")
                .append(System.lineSeparator())
                .append("|    |  1 |  2 |  3 |  4 |  5 |  6 |  7 |  8 |  9 | 10 |").append("\t\t\t").append("|    |  1 |  2 |  3 |  4 |  5 |  6 |  7 |  8 |  9 | 10 |")
                .append(System.lineSeparator());
        for(int i = 0; i < 10; ++i){
            result.append("+----+----+----+----+----+----+----+----+----+----+----+").append("\t\t\t").append("+----+----+----+----+----+----+----+----+----+----+----+")
                    .append(System.lineSeparator()).append("|  ").append((char) (i +'a')).append(" ");
            for(int j = 0; j < 10; ++j) {
                result.append("|  ").append(board1.getBoard()[j][i].getText()).append(" ");
            }
            result.append("|").append("\t\t\t").append("|  ").append((char) (i +'a')).append(" ");
            for(int j = 0; j < 10; ++j) {
                result.append("|  ").append(board2.getBoard()[j][i].getText()).append(" ");
            }
            result.append("|").append(System.lineSeparator());
        }
        result.append("+----+----+----+----+----+----+----+----+----+----+----+").append("\t\t\t").append("+----+----+----+----+----+----+----+----+----+----+----+");
        return String.valueOf(result);
    }

}