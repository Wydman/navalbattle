package com.SPP;

import java.util.Scanner;


class Main
{
    public static void main( String[] args )
    {
        Scanner sc = new Scanner(System.in);

        System.out.println( "Bienvenue à mon jeu de bataille navale !" );

        System.out.println("Quel est le nom du joueur?");
        User user = new User(sc.nextLine());

        System.out.println("Quelle est la taille du bateau que vous voulez créer?" +System.lineSeparator()+
                "5?" +System.lineSeparator()+ "4?" +System.lineSeparator()+ "3?" +System.lineSeparator()+
                "2?");

        System.out.println(" vous avez créé un bateau de taille " +sc.nextLine()+ ".");

        Coordinates coordinates = new Coordinates(5,1);
        Coordinates coordinates1 = new Coordinates(5,7);
        Ship ship = new Ship(coordinates1,coordinates);

        System.out.println(Display.displayBoards(user.getName(), user.getUserBoard(), user.getOpponentBoard()));
    }

}