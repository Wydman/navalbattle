package com.SPP;

import java.util.ArrayList;
import java.util.List;

public class Ship {
    List<Coordinates> shipCoordinates = new ArrayList<>();

    public Ship(Coordinates coordinates1, Coordinates coordinates2){
        this.placeShip(coordinates1,coordinates2);
    }

    public List<Coordinates> getShipCoordinates() {
        return shipCoordinates;
    }

    private void placeShip(Coordinates coordinates1, Coordinates coordinates2){
        int minx = Math.min(coordinates1.getxCoordinate(),coordinates2.getxCoordinate());
        int maxx = Math.max(coordinates1.getxCoordinate(),coordinates2.getxCoordinate());
        int miny = Math.min(coordinates1.getyCoordinate(),coordinates2.getyCoordinate());
        int maxy = Math.max(coordinates1.getyCoordinate(),coordinates2.getyCoordinate());
        Coordinates shipCurrentCoordinates;
        for(int i = minx; i <= maxx; ++i){
            for(int j = miny; j <= maxy; ++j){
                shipCurrentCoordinates = new Coordinates(i,j);
                shipCurrentCoordinates.changeStatus(State.SHIP);
                shipCoordinates.add(shipCurrentCoordinates);
            }
        }
    }
}
