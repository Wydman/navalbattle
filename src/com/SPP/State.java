package com.SPP;

public enum State {
    VOID(" "),
    SHIP("O"),
    VOID_SHOT("*"),
    SHIP_SHOT("X");

    private String text;

    State(String text){
        this.text = text;
    }

    public String getText() {
        return text;
    }
}