package com.SPP;


public class User {

    private String name;
    private Board userBoard;
    private Board opponentBoard;


    public User(String name){
        this.name = name;
        userBoard = new Board();
        opponentBoard = new Board();
    }

    public String getName() {
        return name;
    }

    public Board getUserBoard() {
        return userBoard;
    }

    public Board getOpponentBoard() {
        return opponentBoard;
    }

    @Override
    public String toString() {
        return Display.displayBoards(name, userBoard, opponentBoard);
    }

    public boolean shoot(Coordinates coordinates){
        boolean result = false;
        switch(userBoard.getBoard()[coordinates.getxCoordinate()][coordinates.getyCoordinate()]){
            case SHIP:
                break;
            case VOID:
            case SHIP_SHOT:
            case VOID_SHOT:
                result = true;
                break;
        }
        return result;
    }

}
